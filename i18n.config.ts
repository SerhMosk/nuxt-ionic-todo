export default defineI18nConfig(() => ({
  legacy: false,
  locale: 'en',
  messages: {
    en: {
      welcome: 'Welcome',
      resources: 'Resources',
      start: 'Get Started',
      back: 'Back',
      tasks: 'Tasks',
      task: 'Task',
      'todo-list': 'Todo list',
      'add-info': 'Use the "Add" button to create a new task',
      'add-item': 'Add item',
      'no-tasks': 'There are no tasks yet',
      'enter-name': 'Enter task name',
    },
    ua: {
      welcome: 'Ласкаво просимо',
      resources: 'Ресурси',
      start: 'Розпочати',
      back: 'Назад',
      tasks: 'Задачі',
      task: 'Завдання',
      'todo-list': 'Список завдань',
      'add-info': 'Використовуйте кнопку «Додати», щоб створити нове завдання',
      'add-item': 'Додати позицію',
      'no-tasks': 'Завдань ще немає',
      'enter-name': 'Введіть назву задачі',
    }
  }
}))
