// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ["@/theme/variables.css"],
  modules: ['@nuxtjs/ionic', '@nuxtjs/i18n'],
  ssr: false,
  ionic: {
    integrations: {
      pwa: true,
      router: true,
      meta: true,
      icons: true
    },
    css: {
      core: true,
      basic: true,
      utilities: true
    },
    config: {
      //
    }
  },
  i18n: {
    vueI18n: './i18n.config.ts'
  }
})
