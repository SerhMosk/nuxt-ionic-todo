import axios from 'axios';

class Api {
    constructor() {
        this.axiosInstance = axios.create({
            baseURL: "http://localhost:3001"
        });
    }

    getAll() {
        return this.axiosInstance.get('/todos');
    }

    create(todo) {
        return this.axiosInstance.post('/todos', todo);
    }

    update(todo) {
        return this.axiosInstance.put('/todos/'+todo.id, todo);
    }

    delete(id) {
        return this.axiosInstance.delete('/todos/'+id);
    }
}

const _api = new Api();

export default _api;
